from utils_psychopy import *
import conn_and_wait_trigger_fmri as conn_fmri
import numpy as np
import itertools
import math
import random
import game_objs
import events

class Experiment:
    def __init__(self, psy):
        self.psy = psy
        self.session_number = int(psy.exp_info["session_number"])
        self.participant_number = int(psy.exp_info["participant_number"])
        print("PARTICIPANT NUMBER: ", self.participant_number)
        self.in_fMRI = psy.exp_info['in_fMRI']
        self.objs = game_objs.GameObjs(self.psy, self.participant_number)  # Psychopy objects (stimuli, responses, etc)
        self.clock_total_time = core.Clock()
        self.clock_onsets = core.Clock()

        # Block-trials config (every block has num_trials_per_block; Total trials = num_blocks*num_trials_per_block)

        # Controls
        if self.in_fMRI:
            self.instruction_keys = {
                "backward_key": "num_1",
                "forward_key": "num_2",
                "exit_key": "escape"
            }
            self.left_decision_key = "num_1"
            self.right_decision_key = "num_2"
            self.control_keys = [self.left_decision_key, self.right_decision_key]
            self.next_screen_key = "space"
            self.press_space_text = ""
        else:
            self.instruction_keys = {
                "backward_key": "left",
                "forward_key": "right",
                "exit_key": "escape"
            }
            self.left_decision_key = "q"
            self.right_decision_key = "p"
            self.control_keys = [self.left_decision_key, self.right_decision_key]
            reload_keys_and_text = {"s": "¡Presiona 'S' para recargar!",
                                    "l":  "¡Presiona 'L' para recargar!"}
            self.reload_keys_and_text = list(reload_keys_and_text.items())
            reload_keys_indices = self.counterbalancing_reload_key()
            self.reload_keys_and_text[:] = [self.reload_keys_and_text[i] for i in reload_keys_indices]
            self.reload_keys_and_text = dict(self.reload_keys_and_text)
            self.reload_keys = list(self.reload_keys_and_text.keys())
            self.reload_keys_text = list(self.reload_keys_and_text.values())
            self.next_screen_key = "space"
            self.press_space_text = "Presiona espacio para continuar."

        if self.session_number == 0:
            self.num_trials_per_block = 5  # 44 Minimum recommended: 9 because it allows stims_overtraining to appear 3.5 times more than stims_undertraining. Num_trials less than 22 can
            # cause num_trials to be a little different than the input number here, to try to adjust the number of stims
            # per trial according to the calculate_trials_stims_indices function
            self.num_trials_per_practice_block = 8

            self.consumption_trial_every = 0
            self.reload_trial_every = 3
            self.reload_trial_dev_every = 9999
            self.num_trials_per_block_dev = 4

            self.training_per_over_stim = 0.5
            self.training_times_over_time = 3.5
            self.training_consumption_trials = False
            self.change_side_percentage = 0.5
            self.dev_per_over_stim = 0.5
            self.dev_times_over_time = 1
            self.dev_consumption_trials = False

            self.blocks_design = [
                #"practice",
                "training",
                "dev_soft",
                #                "training",
                "dev",
                "training",
            ]

            # Duration screens:
            self.duration_fixation = 1
            self.duration_fixation_consumption_trial = 0.25
            self.duration_instr_consumption_trial = 1.5
            self.duration_instr_no_dev = 2.0
            self.duration_till_show_responses_imgs = 0.25  # 0.2
            self.duration_between_response_outcome = 0.3  # 2
            self.duration_between_response_outcome_consumption_trial = 0.3  # + np.random.uniform(0, self.duration_max_jitter)
            self.duration_outcome_presentation = 1.2  # 1.4
            self.duration_iti_before_trial = 0.5  # ISI (inter-stimulus interval)
            self.duration_iti_before_consumption_trial = 3  # ISI (inter-stimulus interval)
            self.duration_iti_before_trial_dev = self.duration_iti_before_trial
            self.duration_iti_before_trial_dev_consumption_trial = self.duration_iti_before_trial
            self.duration_error_key_reload = 2
            self.duration_error_late_reload = 1.2
            self.duration_stim_practice = 10
            self.duration_end_game_screen = 10
            # Total duration between trials must be 5 for training blocks and 6 in dev blocks, to allow clean signals in
            # fMRI
            self.duration_total_between_trials = self.duration_fixation + self.duration_till_show_responses_imgs + \
                                                 self.duration_between_response_outcome + \
                                                 self.duration_outcome_presentation + self.duration_iti_before_trial
            self.duration_max_jitter = 0
        elif self.session_number <= 2:
            self.num_trials_per_block = 44  # 44 # Minimum recommended: 9 because it allows stims_overtraining to appear 3.5 times more than stims_undertraining. Num_trials less than 22 can
            # cause num_trials to be a little different than the input number here, to try to adjust the number of stims
            # per trial according to the calculate_trials_stims_indices function
            self.num_trials_per_practice_block = 10  # 10

            self.consumption_trial_every = 0
            self.reload_trial_every = 3
            self.reload_trial_dev_every = 9999
            self.num_trials_per_block_dev = 6  # Mini-dev blocks as a test to warn participants to remember the weapons position

            self.training_per_over_stim = 0.5
            self.training_times_over_time = 3.5
            self.training_consumption_trials = False
            self.change_side_percentage = 0.5
            self.dev_per_over_stim = 0.5
            self.dev_times_over_time = 1
            self.dev_consumption_trials = False

            if self.session_number == 1:
                self.blocks_design = [
                    "practice",
                    "training",
                    "training",
#                    "dev_soft",
                    "training",
#                    "dev",
                    "training",
                ]
            elif self.session_number == 2:
                self.blocks_design = [
                    "training",
                    "training",
#                    "dev_soft",
                    "training",
#                    "dev",
                    "training",
                ]

            # Duration screens:
            self.duration_fixation = 1
            self.duration_fixation_consumption_trial = 0.25
            self.duration_instr_consumption_trial = 1.5
            self.duration_instr_no_dev = 2.0
            self.duration_till_show_responses_imgs = 0.25  # 0.2
            self.duration_between_response_outcome = 0.3  # 2
            self.duration_between_response_outcome_consumption_trial = 0.3  # + np.random.uniform(0, self.duration_max_jitter)
            self.duration_outcome_presentation = 1.2  # 1.4
            self.duration_iti_before_trial = 0.5  # ISI (inter-stimulus interval)
            self.duration_iti_before_consumption_trial = 3  # ISI (inter-stimulus interval)
            self.duration_iti_before_trial_dev = self.duration_iti_before_trial
            self.duration_iti_before_trial_dev_consumption_trial = self.duration_iti_before_trial
            self.duration_error_key_reload = 2
            self.duration_error_late_reload = 1.2
            self.duration_stim_practice = 10
            self.duration_end_game_screen = 10
            # Total duration between trials must be 5 for training blocks and 6 in dev blocks, to allow clean signals in
            # fMRI
            self.duration_total_between_trials = self.duration_fixation + self.duration_till_show_responses_imgs + \
                                                 self.duration_between_response_outcome + \
                                                 self.duration_outcome_presentation + self.duration_iti_before_trial
            self.duration_max_jitter = 0
        elif self.session_number == 3:
            self.num_trials_per_block = 44  # 44 Minimum recommended: 9 because it allows stims_overtraining to appear 3.5 times more than stims_undertraining. Num_trials less than 22 can
            # cause num_trials to be a little different than the input number here, to try to adjust the number of stims
            # per trial according to the calculate_trials_stims_indices function
            self.num_trials_per_practice_block = 10

            self.consumption_trial_every = 0
            self.reload_trial_every = 3
            self.reload_trial_dev_every = 3
            self.num_trials_per_block_dev = self.num_trials_per_block

            self.training_per_over_stim = 0.5
            self.training_times_over_time = 3.5
            self.training_consumption_trials = False
            self.change_side_percentage = 0.5
            self.dev_per_over_stim = 0.5
            self.dev_times_over_time = 1
            self.dev_consumption_trials = False

            self.blocks_design = [
                "practice",  # New training blocks for the experiment with only 1 session
                "training",  # New training blocks for the experiment with only 1 session
                "training",  # New training blocks for the experiment with only 1 session
                "training",  # New training blocks for the experiment with only 1 session
                "training",  # New training blocks for the experiment with only 1 session
                "training",
                "training",
                "dev_soft",
                "training",
                "dev_soft",
                "training",
                "dev",
                "training",
            ]

            # Setup for study with 3 sessions:
            # self.blocks_design = [
            #     "training",
            #     "training",
            #     "dev_soft",
            #     "training",
            #     "dev_soft",
            #     "training",
            #     "dev",
            #     "training",
            # ]

            # Duration screens:
            self.duration_fixation = 1
            self.duration_fixation_consumption_trial = 0.25
            self.duration_instr_consumption_trial = 1.5
            self.duration_instr_no_dev = 2.0
            self.duration_till_show_responses_imgs = 0.25  # 0.2
            self.duration_between_response_outcome = 0.3  # 2
            self.duration_between_response_outcome_consumption_trial = 0.3  # + np.random.uniform(0, self.duration_max_jitter)
            self.duration_outcome_presentation = 1.2  # 1.4
            self.duration_iti_before_trial = 0.5  # ISI (inter-stimulus interval)
            self.duration_iti_before_consumption_trial = 3  # ISI (inter-stimulus interval)
            self.duration_iti_before_trial_dev = self.duration_iti_before_trial
            self.duration_iti_before_trial_dev_consumption_trial = self.duration_iti_before_trial
            self.duration_error_key_reload = 2
            self.duration_error_late_reload = 1.2
            self.duration_stim_practice = 10
            self.duration_end_game_screen = 10
            # Total duration between trials must be 5 for training blocks and 6 in dev blocks, to allow clean signals in
            # fMRI
            self.duration_total_between_trials = self.duration_fixation + self.duration_till_show_responses_imgs + \
                                                 self.duration_between_response_outcome + \
                                                 self.duration_outcome_presentation + self.duration_iti_before_trial
            self.duration_max_jitter = 0

        self.num_blocks = len(self.blocks_design)
        self.max_consecutive_stims = 2
        self.num_overtraining_stim, self.num_undertraining_stim = self.overtraining_stims_stats(self.training_per_over_stim)
        self.stim_overtraining_indices = list(range(self.num_overtraining_stim))
        self.stim_undertraining_indices = list(range(self.num_overtraining_stim, self.num_undertraining_stim))
        self.blocks_config_indices = self.calculate_blocks_config_indices()

        # Trial types
        self.trial_type_config = {
            0: "trial",
            1: "consumption_trial",
        }
        self.trial_type = 0

        # Outcomes config
        self.s_r_o_hashmap = {  # Design table: Stimulus id -> Response id -> outcome id
            0: {  # S0 (overtrained)
                0: 0,  # R0 -> O(100)
                1: 1,  # R1 -> O(5)
            },
            1: {  # S1 (overtrained)
                3: 1,  # R0 -> O(5)
                2: 0,  # R1 -> O(100)
            },
            2: {  # S2 (undertrained)
                2: 0,  # R0 -> O(100)
                3: 1,  # R1 -> O(5)
            },
            3: {  # S3 (undertrained)
                1: 1,  # R0 -> O(5)
                0: 0,  # R1 -> O(100)
            },
        }

        # Devaluation config (for an entire block)
        self.total_dev_levels = len(self.objs.o_max_points_indices) + 1  # +1 to include the no dev option
        self.dev_config = {
            "new_points": 0,
        }

        self.current_outcome_dev = None
        self.outcome_dev_idx = -1  # No devaluation

        # Response time pressure
        self.min_response_time = 0
        self.max_response_time_low = 1  # It was 0.85
        self.max_response_time_reload = 1
        self.response_in_time_map = {
            0: "in time",
            1: "too fast",
            2: "too_slow"
        }
        self.time_pressure_hashmap = {
            0: self.max_response_time_low,
        }
        self.time_pressure_level = int(psy.exp_info["time_pressure"])
        self.max_response_time = self.time_pressure_hashmap[self.time_pressure_level]
        self.max_response_consumption_trial = 4

        self.duration_fast_trial = self.duration_fixation + self.duration_till_show_responses_imgs + \
                                   0.45 + self.duration_between_response_outcome + \
                                   self.duration_outcome_presentation + self.duration_iti_before_trial
        self.duration_fast_trial_reload = 0.7
        self.bonus_points_threshold_accuracy = 0.7

        # Participant results
        self.results = {
            "num_block": [],
            "num_trial": [],
            "curr_block_type": [],
            "trial_type": [],
            "stim_id": [],
            "is_overtraining_stim": [],
            "response_id": [],
            "response_accuracy": [],
            "response_time": [],
            "response_in_time": [],
            "points_this_trial": [],
            "points_max_possible_this_trial": [],
            "points_this_block": [],
            "bonus_points_this_block": [],
            "bonus_points_threshold_time": [],
            "bonus_points_threshold_accuracy": [],
            "duration_block": [],
            "outcome_selected_id": [],
            "outcome_not_selected_id": [],
            "outcome_dev_id": [],
            "outcome_dev_selected": [],
            "response_switch": [],
            "reload_dev": [],
            "reload_correct_key": [],
            "reload_key": [],
            "reload_rt": [],
            "reload_is_late": [],
            "reload_accuracy": [],
            "dev_soft_change_side_trial": [],
            "onset_iti": [],
            "onset_fixation": [],
            "onset_stimulus": [],
            "onset_response_window": [],
            "onset_between_response_outcome": [],
            "onset_outcome_presentation": [],
            "onset_end_trial": [],
            "total_time_end_trial": [],
            "total_points": [],
        }

    def run(self):
        self.objs.draw_welcome_game_info(self.session_number)
        self.psy.win.flip()
        key_pressed, rt = events.get_response_first_key(allowed_keys=["escape", "space"])
        if key_pressed == "escape":
            psychopy.core.quit()

        for i, outcome_dev_idx in enumerate(self.blocks_config_indices):
            self.outcome_dev_idx = outcome_dev_idx
            self.curr_block_type = self.blocks_design[i]
            self.block(i)

        self.objs.draw_end_game_info(self.results["total_points"][-1],
                                     session_number=self.session_number, in_fMRI=self.in_fMRI)
        self.psy.win.flip()
        psychopy.core.wait(self.duration_end_game_screen)

    def block(self, num_block):
        for outcome in self.objs.outcomes:  # Reset original points
            outcome.reset_dev(self.psy)

        if self.curr_block_type == "practice":
            self.trials_stims_indices, self.change_side_indices = self.calculate_trials_stims_indices(self.num_trials_per_practice_block,
                                                                                                      self.training_per_over_stim,
                                                                                                      times_over_stim=1,
                                                                                                      consumption_trials=False)
            self.trials_stims_indices = sorted(self.trials_stims_indices)
            trials_stims_indices = self.trials_stims_indices
            self.current_outcome_dev = None
            self.objs.instr_block_practice.text = self.objs.instr_block_practice_text + '\n\n {}'.format(self.press_space_text)
            self.objs.instr_block_practice.draw()
            self.psy.win.flip()
            psychopy.core.wait(self.duration_instr_no_dev)
        elif self.outcome_dev_idx == -1:  # No devaluation
            num_trials_per_block = self.num_trials_per_block
            if self.curr_block_type == "dev_soft":
                num_trials_per_block = self.num_trials_per_block_dev
                self.trials_stims_indices_dev, self.change_side_indices = self.calculate_trials_stims_indices(
                    self.num_trials_per_block_dev,
                    self.dev_per_over_stim,
                    self.dev_times_over_time,
                    self.dev_consumption_trials,
                    self.outcome_dev_idx)
                trials_stims_indices = self.trials_stims_indices_dev
            else:
                self.trials_stims_indices, self.change_side_indices = self.calculate_trials_stims_indices(num_trials_per_block,
                                                                                                          self.training_per_over_stim,
                                                                                                          self.training_times_over_time,
                                                                                                          self.training_consumption_trials,
                                                                                                          self.outcome_dev_idx)

                trials_stims_indices = self.trials_stims_indices

            self.current_outcome_dev = None
            self.objs.instr_block_no_dev.text = self.objs.instr_block_no_dev_text + '\n\n {}'.format(self.press_space_text)
            self.objs.instr_block_no_dev.draw()
            self.psy.win.flip()
            psychopy.core.wait(self.duration_instr_no_dev)
        else:
            self.trials_stims_indices_dev, self.change_side_indices = self.calculate_trials_stims_indices(self.num_trials_per_block_dev,
                                                                                                          self.dev_per_over_stim,
                                                                                                          self.dev_times_over_time,
                                                                                                          self.dev_consumption_trials,
                                                                                                          self.outcome_dev_idx)
            trials_stims_indices = self.trials_stims_indices_dev
            outcome_dev = self.objs.outcomes[self.outcome_dev_idx]
            outcome_dev.devaluate(self.psy, self.dev_config["new_points"])
            self.objs.instr_block_dev.text = "\n¡La policía intergaláctica ahora prohíbe matar a los aliens!" \
                                             "\n\n ¡¡Debes herirlos para capturarlos con vida!! " \
                                             "\n\n\n ¡Matarlos te penalizará! " \
                                             "\n\n {}".format(self.press_space_text)
            # "\n\n Verás '??' en lugar de saber si les mataste o les capturaste." \

            # outcome_dev.img.draw()
            self.current_outcome_dev = outcome_dev
            self.objs.instr_block_dev_title.draw()
            self.objs.instr_block_dev.draw()

            self.psy.win.flip()

        points_this_block = 0
        events.get_response_wait([self.next_screen_key])

        if self.in_fMRI:
            events.get_response_wait([self.next_screen_key])  # Second confirmation key press
            self.objs.waiting_fmri_text.draw()
            self.psy.win.flip()
            new_clock_total_time = conn_fmri.conn_and_wait_for_MRI_trigger_dsr()
            if num_block == 0:
                self.clock_total_time = new_clock_total_time
                print("TIME reset: ", self.clock_total_time.getTime())

        # print("curr_block_type", self.curr_block_type)
        # print("outcome_dev_idx", self.outcome_dev_idx)
        # print("trials_stims_indices", self.trials_stims_indices)
        i_reload = 0
        accuracy_total_block = 0
        time_init_block = None
        for j, stim_idx in enumerate(trials_stims_indices):  # Each trial must contain at least one trial of each stimuli?
            change_side = self.change_side_indices[j]
            if len(self.results["num_trial"]) == 0:
                last_num_trial = 0
            else:
                last_num_trial = self.results["num_trial"][-1]
            num_trial = last_num_trial+1

            reload_dev = False
            reload_correct_key = None
            reload_key = None
            reload_rt = None
            reload_accuracy = 0
            reload_is_late = False
            if j % self.reload_trial_every == 0:
                i_reload += 1
                reload_dev = False
                if i_reload % self.reload_trial_dev_every == 0:
                    reload_dev = True
                reload_correct_key, reload_key, reload_rt, reload_is_late = self.reload(reload_dev)

            if stim_idx != -1:
                self.trial(stim_idx, change_side, num_trial)
            else:
                self.consumption_trial(num_trial)

            if reload_is_late:
                if len(self.results["total_points"]) > 0:
                    self.results["total_points"][-1] -= 200
                else:
                    self.results["total_points"] = [-200]
            elif reload_key != reload_correct_key:
                if len(self.results["total_points"]) > 0:
                    self.results["total_points"][-1] -= 200
                else:
                    self.results["total_points"] = [-200]
            else:
                if reload_correct_key is None:
                    reload_accuracy = -1
                elif reload_correct_key == reload_key:
                    reload_accuracy = 1

            accuracy_total_block += self.results["response_accuracy"][-1]
            self.results["num_block"].append(num_block)
            self.results["num_trial"].append(num_trial)
            self.results["curr_block_type"].append(self.curr_block_type)
            self.results["outcome_dev_id"].append(self.outcome_dev_idx)
            self.results["trial_type"].append(self.trial_type)
            points_this_block += self.results["points_this_trial"][-1]
            self.results["points_this_block"].append(points_this_block)
            self.results["bonus_points_this_block"].append(0)
            self.results["bonus_points_threshold_time"].append(0)
            self.results["bonus_points_threshold_accuracy"].append(0)
            self.results["duration_block"].append(0)
            self.results["reload_dev"].append(reload_dev)
            self.results["reload_correct_key"].append(reload_correct_key)
            self.results["reload_key"].append(reload_key)
            self.results["reload_rt"].append(reload_rt)
            self.results["reload_is_late"].append(reload_is_late)
            self.results["reload_accuracy"].append(reload_accuracy)
            self.results["dev_soft_change_side_trial"].append(change_side)
            self.results["total_time_end_trial"].append(self.clock_total_time.getTime())
            if j == 0:
                time_init_block = self.results["onset_iti"][-1]
            self.psy.save_results(self.results, format="csv", tmp=True)


        time_end_this_block = self.results["onset_end_trial"][-1]
        duration_block = time_end_this_block - time_init_block
        reload_trials_this_block = math.floor(len(trials_stims_indices) / self.reload_trial_every) + 1  # We count the initial one
        self.duration_fast_block = self.duration_fast_trial * len(trials_stims_indices) +\
                                   self.duration_fast_trial_reload * reload_trials_this_block
        response_accuracy_this_block = accuracy_total_block / len(trials_stims_indices)
        bonus_fast_points = False
        if duration_block < self.duration_fast_block and \
                response_accuracy_this_block >= self.bonus_points_threshold_accuracy:
            bonus_fast_points = True
            self.results["bonus_points_this_block"][-1] = 1

        self.results["duration_block"][-1] = duration_block
        self.results["bonus_points_threshold_time"][-1] = self.duration_fast_block
        self.results["bonus_points_threshold_accuracy"][-1] = self.bonus_points_threshold_accuracy

        # Show temp results to the user
        self.objs.results_block_info.text = "¡Bloque {} de {} terminado! \n\n " \
                                            "Precisión del bloque: {} de {} ({} %) respuestas óptimas. \n" \
                                            "Duración del bloque: {} segundos. \n" \
                                            "Puntos en este bloque: {} (sin contar penalizaciones). \n" \
                                            "Puntos totales: {} \n\n".format(num_block+1, self.num_blocks,
                                                                             accuracy_total_block,
                                                                             len(trials_stims_indices),
                                                                             round(response_accuracy_this_block*100, 1),
                                                                             round(duration_block,1),
                                                                             self.results["points_this_block"][-1],
                                                                             self.results["total_points"][-1])

        if self.curr_block_type != "practice":
            self.objs.results_block_info.text += "(Si tardas menos de {} segundos en completar el bloque, con al menos un {} % de precisión," \
                                                 " recibes un bonus). \n\n\n".format(round(self.duration_fast_block,1),
                                                                                     self.bonus_points_threshold_accuracy*100)
            if bonus_fast_points:
                self.objs.results_block_info.text += "\n\n Ganas bonus de 500 puntos por ser RÁPIDO y PRECISO en este bloque. ¡Intenta seguir mejorando! \n\n "
                self.results["total_points"][-1] += 500
            else:
                self.objs.results_block_info.text += "\n\n ¡Deberás ser más RÁPIDO y PRECISO si quieres ganar un bonus de puntos! \n\n"

        self.objs.results_block_info.text += "\n\n {}".format(self.press_space_text)

        self.objs.results_block_info.draw()
        self.psy.win.flip()

        if self.outcome_dev_idx != -1:
            self.objs.results_block_dev_info.text = '\n ¡Recuerda que estaba prohibido matar a los aliens! \n\n' \
                                                    'Pero lee las instrucciones del siguiente bloque para ver si puedes volver a matarlos.' \
                                                    '\n\n {}'.format(self.press_space_text)
            self.objs.results_block_end_info.draw()
            self.objs.results_block_dev_info.draw()
            # self.current_outcome_dev.img.draw()
            self.psy.win.flip()

        self.psy.save_results(self.results, format="csv", tmp=True)
        events.get_response_wait([self.next_screen_key])
        logging.flush()

    def trial(self, stim_idx, change_side, num_trial, show_countdown_text=False):
        self.trial_type = 0
        onsets = {}

        if num_trial == 1:
            self.clock_onsets.reset()

        onsets["iti"] = self.wait_iti(num_trial, show_countdown_text)

        # Fixation
        self.objs.fixation_stim.draw()
        onsets["fixation"] = self.clock_onsets.getTime()
        self.psy.win.flip()
        psychopy.core.wait(self.duration_fixation)
        psychopy.event.clearEvents()

        # Stimulus img
        # stim_idx = np.random.randint(0, self.objs.total_stims)
        self.objs.stims[stim_idx].img.autoDraw = True
        onsets["stimulus"] = self.clock_onsets.getTime()
        self.psy.win.flip()

        # Delay between stim and response imgs
        ok_response, response_idx, response_time = events.get_response(self.control_keys, True, self.duration_till_show_responses_imgs)
        in_time_code = events.check_response_in_time(response_time, self.duration_till_show_responses_imgs, self.max_response_time)

        outcome_idx = -1
        points_earned = 0
        response_accuracy = 0
        outcome_not_selected_idx = -1
        max_poss_points = 0
        onsets["response_window"] = self.clock_onsets.getTime()
        onsets["between_response_outcome"] = self.clock_onsets.getTime()

        change_side_imgs = False
        if change_side == 1:
            change_side_imgs = True
        if in_time_code == 0:  # Response in time
            # Response imgs
            response_imgs = list(self.s_r_o_hashmap[stim_idx].keys())

            if self.curr_block_type == "dev_soft":
                # print("Change side ", change_side_imgs)
                if change_side_imgs:
                    # print("CHANGING")
                    response_imgs = [response_imgs[1], response_imgs[0]]

            for idx in response_imgs:
                img = self.objs.responses[idx].img
                self.setup_response_img(change_side_imgs, img)
                img.autoDraw = True

            if self.curr_block_type == "practice":
                self.objs.instr_practice_block_trial.autoDraw = True
                self.show_countdown(self.duration_stim_practice)
                self.objs.instr_practice_block_trial.autoDraw = False
                self.objs.instr_practice_block_trial_start.draw()

            onsets["response_window"] = self.clock_onsets.getTime()
            self.psy.win.flip()
            if self.curr_block_type == "practice":
                psychopy.event.clearEvents()

            # Take response
            ok_key, response_idx, response_time = events.get_response(self.control_keys)
            if change_side_imgs:  # Invert the response in dev_soft to get the proper reward
                if response_idx == 0:
                    response_idx = 1
                else:
                    response_idx = 0

            onsets["between_response_outcome"] = self.clock_onsets.getTime()
            if self.curr_block_type == "practice":
                max_response_time = 99
            else:
                max_response_time = self.max_response_time
            in_time_code = events.check_response_in_time(response_time, self.min_response_time,
                                                         max_response_time)

            for idx in response_imgs:
                img = self.objs.responses[idx].img
                img.autoDraw = False
                self.setup_response_img_reset(change_side_imgs, img)
            self.psy.win.flip()

            if ok_key and in_time_code == 0:  # Response in time
                ok_response = True
                # Outcome img
                response_img_idx = list(self.s_r_o_hashmap[stim_idx].keys())[response_idx]
                outcome_idx = self.s_r_o_hashmap[stim_idx][response_img_idx]
                points_earned, response_accuracy, outcome_not_selected_idx, max_poss_points = self.calculate_points_and_accuracy(
                    ok_response, outcome_idx, stim_idx=stim_idx)
                self.objs.stims[stim_idx].img.autoDraw = False
                self.draw_outcome_chosen(outcome_idx, response_accuracy, self.duration_between_response_outcome)

        events.draw_info_response_time(in_time_code, self.objs.outcome_text_fast, self.objs.outcome_text_slow)
        psychopy.event.clearEvents()
        self.objs.stims[stim_idx].img.autoDraw = False
        self.psy.win.flip()
        onsets["outcome_presentation"] = self.clock_onsets.getTime()
        psychopy.core.wait(self.duration_outcome_presentation)
        self.psy.win.flip()

        onsets["end_trial"] = self.clock_onsets.getTime()

        # Update results
        if change_side_imgs:  # Invert the response again in dev_soft to store the response you actually clicked
            if response_idx == 0:
                response_idx = 1
            else:
                response_idx = 0
        self.update_results(stim_idx, response_idx, outcome_idx, outcome_not_selected_idx, response_time,
                            in_time_code, points_earned, response_accuracy, max_poss_points, onsets)

    def setup_response_img_reset(self, change_side_imgs, img):
        img.pos *= 1

        if img.pos[0] > 0:
            img.flipHoriz = False
        if change_side_imgs:
            img.pos *= -1

        return 0

    def setup_response_img(self, change_side_imgs, img):
        img.pos *= 1

        if change_side_imgs:
            img.pos *= -1
        if img.pos[0] > 0:
            img.flipHoriz = True

        return 0

    def reset_responses_imgs_pos(self, img):
        if img.pos[0] > 0:
            img.flipHoriz = False

    def reload(self, reload_dev):
        # onsets["stimulus"] = self.clock_onsets.getTime()
        if not reload_dev:
            reload_key_text = self.reload_keys_text[0]
            reload_correct_key = self.reload_keys[0]
            reload_img = self.objs.reload_imgs[0]
        else:
            reload_key_text = self.reload_keys_text[1]
            reload_correct_key = self.reload_keys[1]
            reload_img = self.objs.reload_imgs[1]

        reload_img.autoDraw = True
        self.objs.reload_info.text = "\n" + reload_key_text
        self.objs.reload_info.draw()
        self.psy.win.flip()

        reload_key, reload_rt = events.get_response_first_key()

        reload_is_late = False
        if reload_rt > self.max_response_time_reload:
            reload_is_late = True
            self.objs.text_points_late_reload.draw()
            self.psy.win.flip()
            psychopy.core.wait(self.duration_error_late_reload)
        elif reload_key != reload_correct_key:
            self.objs.text_points_error_reload.draw()
            self.psy.win.flip()
            psychopy.core.wait(self.duration_error_key_reload)

        reload_img.autoDraw = False
        self.psy.win.flip()

        return reload_correct_key, reload_key, reload_rt, reload_is_late

    def consumption_trial(self, num_trial, show_countdown_text=False):
        self.trial_type = 1
        onsets = {}

        # Instructions
        self.objs.instr_consumption_trial.draw()
        self.psy.win.flip()
        psychopy.core.wait(self.duration_instr_consumption_trial)

        onsets["iti"] = self.wait_iti(num_trial, show_countdown_text)

        # Fixation
        self.objs.fixation_stim.draw()
        onsets["fixation"] = self.clock_onsets.getTime()
        self.psy.win.flip()
        psychopy.core.wait(self.duration_fixation_consumption_trial)
        psychopy.event.clearEvents()

        # Outcome objs
        possible_outcomes_inds = list(range(self.objs.total_outcomes))
        if self.outcome_dev_idx != -1:
            outcome_inds = list(np.random.choice(self.objs.o_max_points_indices, size=2, replace=False))
        else:
            outcome_inds = list(np.random.choice(len(possible_outcomes_inds), size=2, replace=False))

        self.objs.outcomes_change_pos_consumption(outcome_inds)

        # Arrow
        self.objs.arrows_stim.draw()
        onsets["stimulus"] = self.clock_onsets.getTime()
        self.psy.win.flip()
        psychopy.event.clearEvents()

        # Take response
        onsets["response_window"] = self.clock_onsets.getTime()
        ok_key, response_idx, response_time = events.get_response(self.control_keys)
        onsets["between_response_outcome"] = self.clock_onsets.getTime()
        in_time_code = events.check_response_in_time(response_time, self.min_response_time, self.max_response_consumption_trial)
        events.draw_info_response_time(in_time_code, self.objs.outcome_text_fast, self.objs.outcome_text_slow)

        # Restore outcome original pos
        self.objs.restore_outcomes_pos(outcome_inds)

        ok_response = False
        outcome_idx = -1
        points_earned = 0
        response_accuracy = 0
        outcome_not_selected_idx = -1
        max_poss_points = 0
        if in_time_code == 0:  # Response in time
            ok_response = True
            outcome_idx = outcome_inds[response_idx]
            points_earned, response_accuracy, outcome_not_selected_idx, max_poss_points = self.calculate_points_and_accuracy(
                ok_response, outcome_idx, outcome_inds=outcome_inds)
            self.draw_outcome_chosen(outcome_idx, response_accuracy, self.duration_between_response_outcome_consumption_trial)

        self.psy.win.flip()
        onsets["outcome_presentation"] = self.clock_onsets.getTime()
        psychopy.core.wait(self.duration_outcome_presentation)
        self.psy.win.flip()

        onsets["end_trial"] = self.clock_onsets.getTime()

        # Update results
        stim_idx = -1
        self.update_results(stim_idx, response_idx, outcome_idx, outcome_not_selected_idx, response_time,
                            in_time_code, points_earned, response_accuracy, max_poss_points, onsets)

    def wait_iti(self, num_trial, show_countdown_text):
        duration_iti = self.duration_iti_before_trial

        if self.outcome_dev_idx != -1:
            duration_iti = self.duration_iti_before_trial_dev
        if self.session_number == 3:
            duration_iti += np.random.uniform(0, self.duration_max_jitter)

        onset = self.clock_onsets.getTime()

        self.psy.win.flip()
        if show_countdown_text:
            self.show_countdown(duration_iti)
        else:
            psychopy.core.wait(duration_iti)
        psychopy.event.clearEvents()

        return onset

    def calculate_trials_stims_indices(self, num_trials_per_block, per_over_stim=0.5, times_over_stim=10.0,
                                       consumption_trials=True, outcome_dev_idx=-1):
        if num_trials_per_block < self.objs.total_stims:
            raise Exception("Error: num_trials_per_block ({}) must be greater than total number of stims ({})".format(
                num_trials_per_block, self.objs.total_stims))

        num_overtraining_stim, num_undertraining_stim = self.overtraining_stims_stats(per_over_stim)

        times_appear_overtraining_stim = times_over_stim * num_overtraining_stim  # Overtraining stim appear X times more than undertraining stim
        times_appear_undertraining_stim = 1 * num_undertraining_stim
        total_times_appear_stim = times_appear_overtraining_stim + times_appear_undertraining_stim

        mult_overtraining_num_stim = num_trials_per_block / total_times_appear_stim
        num_overtraining_stim_per_block = mult_overtraining_num_stim * times_appear_overtraining_stim
        num_overtraining_stim_per_block = math.floor(num_overtraining_stim_per_block / num_overtraining_stim)
        mult_undertraining_num_stim = num_trials_per_block / total_times_appear_stim
        num_undertraining_stim_per_block = mult_undertraining_num_stim * times_appear_undertraining_stim
        num_undertraining_stim_per_block = math.ceil(num_undertraining_stim_per_block / num_undertraining_stim)

        trials_stims_indices = []

        config_stims_per_block = [num_overtraining_stim_per_block] * num_overtraining_stim + \
                                 [num_undertraining_stim_per_block] * num_undertraining_stim

        for i, num_stim_per_block in enumerate(config_stims_per_block):
            stim_indices = [i] * num_stim_per_block
            trials_stims_indices += stim_indices
        random.shuffle(trials_stims_indices)

        # Check for no more than X consecutive repetitions of the same stimulus
        trials_stims_indices = self.shuffle_stims_with_no_max_consecutives(trials_stims_indices)

        if self.outcome_dev_idx == -1:  # No devaluation
            num_trials_per_block = len(trials_stims_indices)
        else:
            num_trials_per_block_dev = len(trials_stims_indices)

        # Consumption trials config:
        if consumption_trials:
            consumption_trials_inds = np.arange(self.consumption_trial_every, num_trials_per_block,
                                                step=self.consumption_trial_every)  # Consumption trial every X normal trials

            for i in range(len(trials_stims_indices)):
                if i in consumption_trials_inds:
                    trials_stims_indices.insert(i, -1)


        # Change side trials:
        if self.curr_block_type == "dev_soft":
            change_side_percentage = self.change_side_percentage
        else:
            change_side_percentage = 0.0  # TODO: change to 0
        num_change_side = round(num_trials_per_block * change_side_percentage)
        num_no_change = num_trials_per_block - num_change_side
        no_change_side_indices = [0] * num_no_change
        change_side_only_indices = [1] * num_change_side

        change_side_indices = no_change_side_indices + change_side_only_indices
        random.shuffle(change_side_indices)
        # Check for no more than X consecutive repetitions of the same stimulus
        # if num_change_side > 0:
        #     change_side_indices = self.shuffle_stims_with_no_max_consecutives(change_side_indices)

        return trials_stims_indices, change_side_indices

    def overtraining_stims_stats(self, per_over_stim):
        percentage_overtraining_stim = per_over_stim  # Overtraining stim are the X % of the total stim (e.g. if X=50% and there are 4 stimuli, 2 of them will be overtrained stim)
        num_overtraining_stim = int(self.objs.total_stims * percentage_overtraining_stim)
        num_undertraining_stim = self.objs.total_stims - num_overtraining_stim

        return num_overtraining_stim, num_undertraining_stim

    def shuffle_stims_with_no_max_consecutives(self, inds):
        are_max_consecutives = True
        i_consecutive = 1

        while are_max_consecutives:
            ind = 1
            while ind < len(inds):
                # print("Checking for max_consecutives in ind {}".format(ind))
                if inds[ind] == inds[ind-1]:
                    i_consecutive += 1
                else:
                    i_consecutive = 1

                if i_consecutive > self.max_consecutive_stims:  # Reallocate this 3rd consecutive stimulus to not be consecutive
                    # print("Trying to reallocate ind {}".format(ind))
                    insertion_possible = False
                    val_reallocate = inds[ind]
                    while not insertion_possible:
                        rand_ind = np.random.randint(0, len(inds))
                        insertion_possible = self.check_if_max_stim_consecutive_around(val_reallocate, rand_ind, inds)
                    val_substitute = inds[rand_ind]
                    inds[rand_ind] = val_reallocate
                    inds[ind] = val_substitute

                    # Reset search
                    ind = 1
                    i_consecutive = 1
                    # print("Resetting search for new_max because of new reallocations")
                else:
                    ind += 1

            are_max_consecutives = False

        return inds

    def check_if_max_stim_consecutive_around(self, val, ind, inds):
        insertion_possible = False

        start_ind = ind - self.max_consecutive_stims + 1 if ind >= self.max_consecutive_stims else 1
        end_ind = start_ind + (self.max_consecutive_stims * 2)
        if end_ind > len(inds):
            end_ind = len(inds)
        i_consecutive = 1
        for i in range(start_ind, end_ind):
            if inds[i] == val:
                i_consecutive += 1
            else:
                i_consecutive = 1

        if i_consecutive < self.max_consecutive_stims:
            insertion_possible = True

        return insertion_possible

    def calculate_blocks_config_indices(self):
        blocks_config_indices = []
        # o_dev_indices = self.counterbalancing_o_dev()
        o_dev_indices = [0]

        count_dev = 0
        for i, type in enumerate(self.blocks_design):
            dev_id = -1  # Training
            if type == "dev":
                if count_dev >= len(o_dev_indices):
                    count_dev = 0
                dev_id = o_dev_indices[count_dev]
                count_dev += 1

            blocks_config_indices.append(dev_id)

        # dev_block_inds_no_consec = []  # No definitve list, this not includes consecutive dev blocks
        # if self.dev_block_every > 0:
        #     dev_block_inds_no_consec = list(np.arange(self.dev_block_every, self.num_blocks, step=self.dev_block_every))
        #
        # dev_block_inds_no_consec = dev_block_inds_no_consec.copy()
        # dev_block_inds = []
        # for dev_block_idx in dev_block_inds_no_consec:
        #     dev_block_inds.append(dev_block_idx)
        #     for j in range(self.num_consecutive_dev_blocks):
        #         dev_block_inds.append(dev_block_idx+j+1)
        #
        # i_dev = 0
        # for i in range(self.num_blocks):
        #     outcome_dev_id = -1
        #     if i in dev_block_inds:
        #         if i_dev >= len(o_dev_indices):
        #             i_dev = 0
        #         outcome_dev_id = o_dev_indices[i_dev]
        #         i_dev += 1
        #     blocks_config_indices.append(outcome_dev_id)

        return blocks_config_indices

    def show_countdown(self, num_seconds=3):
        for i in reversed(range(num_seconds)):
            self.objs.countdown_info.text = str(i+1)
            self.objs.countdown_info.draw()
            self.psy.win.flip()
            psychopy.core.wait(1)

    def draw_outcome_chosen(self, outcome_idx, accuracy, delay):
        duration_between_response_outcome = delay

        if self.session_number == 3:
            duration_between_response_outcome += np.random.uniform(0, self.duration_max_jitter)

        self.black_screen_delay(duration_between_response_outcome)

        self.objs.outcomes[outcome_idx].img.draw()
        if self.trial_type == 1:  # consumption trial
            self.objs.outcomes[outcome_idx].points_obj.draw()
        else:  # Not a consumption trial
            self.objs.show_text_points_info(accuracy, self.curr_block_type)

        return 0

    def calculate_points_and_accuracy(self, ok_response, outcome_idx, stim_idx=None, outcome_inds=[]):
        points_earned = self.objs.outcomes[outcome_idx].points if ok_response else 0
        if self.trial_type == 0:
            response_accuracy, outcome_not_selected_id, max_possible_points = self.accuracy_trial(
                stim_idx, points_earned)
        else:
            response_accuracy, outcome_not_selected_id, max_possible_points = self.accuracy_consumption_trial(
                outcome_inds, points_earned)

        return points_earned, response_accuracy, outcome_not_selected_id, max_possible_points

    def update_results(self, stim_idx, response_idx, outcome_idx, outcome_not_selected_idx, response_time, in_time_code,
                       points_earned, response_accuracy, max_possible_points, onsets):
        outcome_dev_selected = int(self.objs.outcomes[outcome_idx].is_dev)
        is_overtraining_stim = self.check_is_overtraining_stim(stim_idx)

        self.results["stim_id"].append(stim_idx)
        self.results["is_overtraining_stim"].append(is_overtraining_stim)
        self.results["response_id"].append(response_idx)
        self.results["response_accuracy"].append(response_accuracy)
        self.results["response_time"].append(response_time)
        self.results["response_in_time"].append(in_time_code)
        self.results["points_this_trial"].append(points_earned)
        self.results["points_max_possible_this_trial"].append(max_possible_points)
        self.results["outcome_selected_id"].append(outcome_idx)
        self.results["outcome_not_selected_id"].append(outcome_not_selected_idx)
        self.results["outcome_dev_selected"].append(outcome_dev_selected)
        self.results["onset_fixation"].append(onsets["fixation"])
        self.results["onset_stimulus"].append(onsets["stimulus"])
        self.results["onset_response_window"].append(onsets["response_window"])
        self.results["onset_between_response_outcome"].append(onsets["between_response_outcome"])
        self.results["onset_outcome_presentation"].append(onsets["outcome_presentation"])
        self.results["onset_iti"].append(onsets["iti"])
        self.results["onset_end_trial"].append(onsets["end_trial"])

        response_switch = 0
        if self.outcome_dev_idx != -1 and (outcome_not_selected_idx == self.outcome_dev_idx):
            response_switch = 1
        self.results["response_switch"].append(response_switch)

        self.set_total_points()

    def set_total_points(self):
        total_points = self.results["total_points"][-1] if len(self.results["total_points"]) > 0 else 0
        self.results["total_points"].append(total_points + self.results["points_this_trial"][-1])

    def accuracy_trial(self, stim_idx, points_earned):
        accuracy = 0
        max_possible_points = 0
        outcome_not_selected_id = -1

        if stim_idx == -1:
            return accuracy

        for response_idx, outcome_idx in self.s_r_o_hashmap[stim_idx].items():
            possible_outcome = self.objs.outcomes[outcome_idx].points
            if possible_outcome > max_possible_points:
                max_possible_points = possible_outcome
            if points_earned != possible_outcome:
                outcome_not_selected_id = outcome_idx

        if points_earned < max_possible_points:
            accuracy = 0  # Suboptimal response
        else:
            accuracy = 1  # Optimal response

        return accuracy, outcome_not_selected_id, max_possible_points

    def accuracy_consumption_trial(self, outcome_inds, points_earned):
        accuracy = 0
        max_possible_points = 0
        outcome_not_selected_id = -1

        for i, outcome_idx in enumerate(outcome_inds):
            possible_outcome = self.objs.outcomes[outcome_idx].points
            if possible_outcome > max_possible_points:
                max_possible_points = possible_outcome
            if points_earned != possible_outcome:
                outcome_not_selected_id = outcome_idx

        if points_earned < max_possible_points:
            accuracy = 0  # Suboptimal response
        else:
            accuracy = 1  # Optimal response

        return accuracy, outcome_not_selected_id, max_possible_points

    def check_is_overtraining_stim(self, stim_id):
        is_overtraining_stim = 0
        if stim_id in self.stim_overtraining_indices:
            is_overtraining_stim = 1

        return is_overtraining_stim

    def counterbalancing_reload_key(self):
        reload_key_indices = list(range(len(self.reload_keys_and_text)))
        reload_key_indices_combs = list(itertools.permutations(reload_key_indices, len(reload_key_indices)))
        participant_number = self.participant_number
        if participant_number > len(reload_key_indices) - 1:
            participant_number = participant_number - \
                                 math.trunc((participant_number / len(reload_key_indices))) * len(reload_key_indices)

        reload_key_indices = list(reload_key_indices_combs[participant_number])

        return reload_key_indices

    def counterbalancing_o_dev(self):
        o_dev_indices = self.stim_overtraining_indices
        o_dev_indices_combs = list(itertools.permutations(o_dev_indices, len(o_dev_indices)))
        participant_number = self.participant_number
        if participant_number > len(o_dev_indices) - 1:
            participant_number = participant_number - \
                                 math.trunc((participant_number / len(o_dev_indices))) * len(o_dev_indices)

        o_dev_indices = list(o_dev_indices_combs[participant_number])

        return o_dev_indices

    def black_screen_delay(self, time):
        self.psy.win.flip(clearBuffer=True)
        psychopy.core.wait(time)

        return 0
