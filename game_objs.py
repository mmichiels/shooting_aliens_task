from utils_psychopy import *
import numpy as np
import itertools
import math
import random
import psychopy
from psychopy import visual

class GameObjs():
    def __init__(self, psy, participant_number):
        self.psy = psy
        self.participant_number = participant_number

        max_points = 100
        min_points = 5
        self.first_o_points = [max_points, min_points, 0]
        self.o_max_points_indices = []
        self.o_min_points_indices = []
        for i, points in enumerate(self.first_o_points):
            if points == max_points:
                self.o_max_points_indices.append(i)
            elif points == min_points:
                self.o_min_points_indices.append(i)

        self.first_o_names = ["Muerto", "Herido"]
        self.optimal_points_info = {"text": "¡Bien! ¡Has matado al alien!", "color": "white"}
        self.optimal_points_info_dev = {"text": "¡Bien! ¡Has herido al alien!", "color": "white"}
        self.suboptimal_points_info = {"text": "¡Uy! ¡Has herido al alien!", "color": "red"}
        self.suboptimal_points_info_dev = {"text": "¡Nooo! ¡Has matado al alien!", "color": "red"}
        self.normal_text_size = 0.044
        self.small_text_size = 0.03

        self.stims_def_pos = (0, 0)
        self.stims_def_size = (0.19, 0.19)
        self.responses_pos = [(-0.5, 0), (0.5, 0), (-0.5, 0), (0.5, 0)]
        self.responses_def_size = [(0.2, 0.2), (0.2, 0.2), (0.2, 0.2), (0.2, 0.2)]
        self.outcomes_def_pos = (0, 0)
        self.outcomes_def_size = (0.16, 0.16)
        self.outcomes_first_sizes = [(0.16, 0.16), (0.15, 0.15)]
        self.outcomes_pos_consumption_trial = [(-0.3, 0), (0.3, 0)]
        self.num_reload_imgs = 2
        self.reload_img_name = "reload"
        self.reload_pos = (0, 0)
        self.reload_def_size = (0.14, 0.14)

        # self.stims_def_pos = (0, 0)
        # self.stims_def_size = (0.18, 0.18)
        # self.responses_pos = [(-0.5, 0), (0.5, 0)]
        # self.responses_def_size = (0.9, 0.6)
        # self.outcomes_def_pos = (0, 0)
        # self.outcomes_def_size = (0.15, 0.15)
        # self.outcomes_first_sizes = [(0.15, 0.15), (0.153, 0.153), (0.13, 0.13)]
        # self.outcomes_pos_consumption_trial = [(-0.3, 0), (0.3, 0)]

        self.load_game_objs()

    def load_game_objs(self):
        self.total_stims = 0
        self.total_responses = 0
        self.total_outcomes = 0
        stim_init_str = "stim_"
        response_init_str = "response_"
        outcome_init_str = "outcome_"
        img_extension = ".png"

        for img_filename in os.listdir(self.psy.dir_game_imgs):
            if img_filename.startswith(stim_init_str):
                self.total_stims += 1
                if self.total_stims == 1:
                    img_name, img_extension = os.path.splitext(img_filename)
            elif img_filename.startswith(response_init_str):
                self.total_responses += 1
            elif img_filename.startswith(outcome_init_str):
                self.total_outcomes += 1

        s_comb, r_comb, o_comb = self.counterbalancing_s_r_o()

        self.stims = self.create_stims(s_comb, img_extension, stim_init_str)
        self.responses = self.create_responses(r_comb, img_extension, response_init_str)
        self.outcomes = self.create_outcomes(o_comb, img_extension, outcome_init_str)
        self.reload_imgs = self.create_reload()

        self.fixation_stim = visual.Circle(win=self.psy.win, radius=0.01, color="red")
        # name='fixation',
        #                                               text='.',
        #                                               font='Arial',
        #                                               units='height', pos=(0, 0), height=0.4, wrapWidth=None, ori=0,
        #                                               color='red', colorSpace='rgb', opacity=1,
        #                                               languageStyle='LTR',
        #                                               depth=0.0)


        self.waiting_fmri_text = visual.TextStim(win=self.psy.win, name='waiting_fmri',
                                                 text='Esperando resonancia...',
                                                 font='Arial',
                                                 units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                 color='white', colorSpace='rgb', opacity=1,
                                                 languageStyle='LTR',
                                                 depth=0.0)

        self.instr_consumption_trial = visual.TextStim(win=self.psy.win, name='instr_consumption_trial',
                                                       text='Los aliens están distraídos...',
                                                       font='Arial',
                                                       units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                       color='white', colorSpace='rgb', opacity=1,
                                                       languageStyle='LTR',
                                                       depth=0.0)

        arrow_stim_img_filepath = os.path.join(self.psy.dir_game_imgs, "arrows" + img_extension)
        self.arrows_stim = self.psy.load_img_from_disk(arrow_stim_img_filepath, size=(0.2, 0.2))

        self.outcome_points_unknown = visual.TextStim(win=self.psy.win, name='outcome_points_unknown',
                                                      text='??',
                                                      font='Arial',
                                                      units='height', pos=(0, -0.1), height=0.04, wrapWidth=None, ori=0,
                                                      color='white', colorSpace='rgb', opacity=1,
                                                      languageStyle='LTR',
                                                      depth=0.0)

        self.outcome_text_slow = visual.TextStim(win=self.psy.win, name='outcome_text_slow',
                                                 text='¡Muy lento! \n El alien ha huido.',
                                                 font='Arial',
                                                 units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                 color='white', colorSpace='rgb', opacity=1,
                                                 languageStyle='LTR',
                                                 depth=0.0)

        self.outcome_text_fast = visual.TextStim(win=self.psy.win, name='outcome_text_fast',
                                                 text='¡DEMASIADO RÁPIDO! \n ¡El alien te ha esquivado!',
                                                 font='Arial',
                                                 units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                 color='white', colorSpace='rgb', opacity=1,
                                                 languageStyle='LTR',
                                                 depth=0.0)

        text_intro_new_block = ""  # Va a empezar un nuevo bloque. \n\n"
        self.instr_block_no_dev_text = '¡Acaba con todos los aliens! \n\n ' \
                                       'Matar: 100 puntos. \n' \
                                       ' Herir: 5 puntos. \n' \
                                       'Pulsa la Q para seleccionar el arma de la izquierda, y la P para seleccionar la de la derecha. \n\n' \
                                       ' Intenta RECORDAR qué ARMA mata y qué arma solo le HIERE. \n\n' \
                                       ' Si además de ser preciso, eres RÁPIDO, te daremos un buen BONUS de puntos al finalizar cada bloque. \n\n' \
                                       ' Te recomendamos que prepares el dedo corazón (el del medio) de cada mano en la q y p ' \
                                       'respectivamente; y el dedo índice de cada mano en la S y L.' \
                                       ''
        self.instr_block_practice_text = '¡Acaba con todos los aliens! \n ' \
                                         'Matar: 100 puntos. \n' \
                                         ' Herir: 5 puntos. \n' \
                                         'Pulsa la q para seleccionar el arma de la izquierda, y la p para seleccionar la de la derecha. \n\n' \
                                         ' Intenta RECORDAR qué ARMA mata y qué arma solo le HIERE. \n\n' \
                                         ' Te recomendamos que prepares el dedo corazón (el del medio) de cada mano en la q y p ' \
                                         'respectivamente; y el dedo índice de cada mano en la S y L. \n\n' \
                                         'Este primer bloque será solo de práctica para que aprendas la mecánica del juego.' \
                                         ''
        self.instr_block_no_dev = visual.TextStim(win=self.psy.win, name='instr_block_no_dev',
                                                  text=text_intro_new_block + self.instr_block_no_dev_text, font='Arial',
                                                  units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                  color='white', colorSpace='rgb', opacity=1,
                                                  languageStyle='LTR',
                                                  depth=0.0)

        self.instr_block_practice = visual.TextStim(win=self.psy.win, name='instr_block_practice',
                                                    text=text_intro_new_block + self.instr_block_practice_text, font='Arial',
                                                    units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                    color='white', colorSpace='rgb', opacity=1,
                                                    languageStyle='LTR',
                                                    depth=0.0)
        self.instr_practice_block_trial = visual.TextStim(win=self.psy.win, name='instr_block_no_dev',
                                                          text="Te dejamos unos segundos para que observes la forma de "
                                                               "ambas armas detenidamente. \n\n "
                                                               "Intenta averiguar cuál matará a este alien.", font='Arial',
                                                          units='height', pos=(0, 0.3), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                          color='white', colorSpace='rgb', opacity=1,
                                                          languageStyle='LTR',
                                                          depth=0.0)
        self.instr_practice_block_trial_start = visual.TextStim(win=self.psy.win, name='instr_block_no_dev',
                                                                text="¡Escoge el arma que crees que \n matará a este alien!", font='Arial',
                                                                units='height', pos=(0, 0.2), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                                color='white', colorSpace='rgb', opacity=1,
                                                                languageStyle='LTR',
                                                                depth=0.0)

        self.instr_block_dev_title = visual.TextStim(win=self.psy.win, name='instr_block_dev_title',
                                                     text=text_intro_new_block + '¡¡CUIDADO!!',
                                                     font='Arial',
                                                     units='height', pos=(0, 0.3), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                     color='white', colorSpace='rgb', opacity=1,
                                                     languageStyle='LTR',
                                                     depth=0.0)

        self.instr_block_dev = visual.TextStim(win=self.psy.win, name='instr_block_dev',
                                               text='',
                                               font='Arial',
                                               units='height', pos=(0, -0.1), height=self.normal_text_size, wrapWidth=None, ori=0,
                                               color='white', colorSpace='rgb', opacity=1,
                                               languageStyle='LTR',
                                               depth=0.0)

        self.text_points_obj_optimal = visual.TextStim(win=self.psy.win, name='text_points_obj_optimal',
                                                       text=self.optimal_points_info["text"],
                                                       font='Arial',
                                                       units='height', pos=(0, -0.15), height=self.normal_text_size, wrapWidth=None,
                                                       ori=0,
                                                       color=self.optimal_points_info["color"], colorSpace='rgb', opacity=1,
                                                       languageStyle='LTR',
                                                       depth=0.0)
        self.text_points_obj_optimal_dev = visual.TextStim(win=self.psy.win, name='text_points_obj_optimal',
                                                           text=self.optimal_points_info_dev["text"],
                                                           font='Arial',
                                                           units='height', pos=(0, -0.15), height=self.normal_text_size, wrapWidth=None,
                                                           ori=0,
                                                           color=self.optimal_points_info_dev["color"], colorSpace='rgb', opacity=1,
                                                           languageStyle='LTR',
                                                           depth=0.0)

        self.text_points_obj_suboptimal = visual.TextStim(win=self.psy.win, name='text_points_obj_suboptimal',
                                                          text=self.suboptimal_points_info["text"],
                                                          font='Arial',
                                                          units='height', pos=(0, -0.15), height=self.small_text_size, wrapWidth=None,
                                                          ori=0,
                                                          color=self.suboptimal_points_info["color"], colorSpace='rgb', opacity=1,
                                                          languageStyle='LTR',
                                                          depth=0.0)
        self.text_points_obj_suboptimal_dev = visual.TextStim(win=self.psy.win, name='text_points_obj_suboptimal',
                                                              text=self.suboptimal_points_info_dev["text"],
                                                              font='Arial',
                                                              units='height', pos=(0, -0.15), height=self.small_text_size, wrapWidth=None,
                                                              ori=0,
                                                              color=self.suboptimal_points_info_dev["color"], colorSpace='rgb', opacity=1,
                                                              languageStyle='LTR',
                                                              depth=0.0)
        self.text_points_late_reload = visual.TextStim(win=self.psy.win, name='text_points_obj_suboptimal',
                                                       text="¡Demasiado lento! \n "
                                                            "Se te restan 200 puntos.",
                                                       font='Arial',
                                                       units='height', pos=(0, -0.25), height=self.small_text_size, wrapWidth=None,
                                                       ori=0,
                                                       color="red", colorSpace='rgb', opacity=1,
                                                       languageStyle='LTR',
                                                       depth=0.0)
        self.text_points_error_reload = visual.TextStim(win=self.psy.win, name='text_points_obj_suboptimal',
                                                        text="La primera tecla que pulsaste para recargar fue incorrecta. \n "
                                                             "Se te restan 200 puntos.",
                                                        font='Arial',
                                                        units='height', pos=(0, -0.25), height=self.small_text_size, wrapWidth=None,
                                                        ori=0,
                                                        color="red", colorSpace='rgb', opacity=1,
                                                        languageStyle='LTR',
                                                        depth=0.0)
        self.results_block_info = visual.TextStim(win=self.psy.win, name='results_block_info',
                                                  text='',
                                                  font='Arial',
                                                  units='height', pos=(0, 0), height=self.small_text_size, wrapWidth=None, ori=0,
                                                  color='white', colorSpace='rgb', opacity=1,
                                                  languageStyle='LTR',
                                                  depth=0.0)

        self.results_block_end_info = visual.TextStim(win=self.psy.win, name='results_block_end_info',
                                                      text='¡Bloque terminado!',
                                                      font='Arial',
                                                      units='height', pos=(0, 0.2), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                      color='white', colorSpace='rgb', opacity=1,
                                                      languageStyle='LTR',
                                                      depth=0.0)

        self.results_block_dev_info = visual.TextStim(win=self.psy.win, name='results_block_dev_info',
                                                      text='' ,
                                                      font='Arial',
                                                      units='height', pos=(0, -0.1), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                      color='white', colorSpace='rgb', opacity=1,
                                                      languageStyle='LTR',
                                                      depth=0.0)

        self.countdown_info = visual.TextStim(win=self.psy.win, name='countdown_info',
                                              text='',
                                              font='Arial',
                                              units='height', pos=(0, 0.14), height=self.normal_text_size, wrapWidth=None, ori=0,
                                              color='white', colorSpace='rgb', opacity=1,
                                              languageStyle='LTR',
                                              depth=0.0)
        self.reload_info = visual.TextStim(win=self.psy.win, name='reload_info',
                                           text='',
                                           font='Arial',
                                           units='height', pos=(0, -0.1), height=self.normal_text_size, wrapWidth=None, ori=0,
                                           color='white', colorSpace='rgb', opacity=1,
                                           languageStyle='LTR',
                                           depth=0.0)

    def draw_welcome_game_info(self, session, in_fMRI=False):
        # For the original study with 3 sessions:
        # self.welcome_game_info = visual.TextStim(win=self.psy.win, name='end_game_info',
        #                                      text='\n\n\n\n\n SESIÓN {}. \n\n\n\n Si te has confundido de sesión pulsa ' \
        #                                           'la tecla Escape (Esc) para salir. \n\n Si quieres continuar, pulsa ' \
        #                                           'el espacio.'.format(session),
        #                                      font='Arial',
        #                                      units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
        #                                      color='white', colorSpace='rgb', opacity=1,
        #                                      languageStyle='LTR',
        #                                      depth=0.0)

        self.welcome_game_info = visual.TextStim(win=self.psy.win, name='end_game_info',
                                             text='\n\n\n\n\n El estudio va a comenzar. \n\n\n\n Si quieres salir y hacerlo en otro momento, pulsa ' \
                                                  'la tecla Escape (Esc) para salir. \n\n Si quieres continuar, pulsa ' \
                                                  'el espacio.',
                                             font='Arial',
                                             units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                             color='white', colorSpace='rgb', opacity=1,
                                             languageStyle='LTR',
                                             depth=0.0)

        self.welcome_game_info.draw()

    def draw_end_game_info(self, total_points, session_number, in_fMRI=False):
        self.end_game_info = visual.TextStim(win=self.psy.win, name='end_game_info',
                                             text='FIN DE LA SESIÓN. \n\n Puntos totales: {} \n\n '
                                                  'Gracias por participar.'.format(total_points),
                                             font='Arial',
                                             units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                             color='white', colorSpace='rgb', opacity=1,
                                             languageStyle='LTR',
                                             depth=0.0)
        if not in_fMRI and session_number == 3:
            self.end_game_info.text += " \n\n Todas las sesiones terminadas. Por favor, ahora lee el PDF con las instrucciones " \
                                       "para enviar tus resultados."

        if session_number == 1:
            self.end_game_info.text += " \n\n No olvides realizar la sesión de mañana y la de pasado mañana."
        if session_number == 2:
            self.end_game_info.text += " \n\n No olvides que mañana debes realizar la última sesión."

        self.end_game_info.draw()

    def counterbalancing_s_r_o(self, randomize_outcomes=False):
        stims_indices = list(range(self.total_stims))
        responses_indices = list(range(self.total_responses))
        outcomes_indices = list(range(self.total_outcomes))

        all_s_r_o_combinations = [stims_indices, responses_indices, outcomes_indices]
        all_s_r_o_combinations = list(itertools.product(*all_s_r_o_combinations))

        s_combinations = shift_list_n_times(stims_indices)
        r_combinations = shift_list_n_times(responses_indices)
        o_combinations = shift_list_n_times(outcomes_indices)

        participant_number = self.participant_number
        if participant_number > len(all_s_r_o_combinations) - 1:
            participant_number = participant_number - \
                                 math.trunc((participant_number / len(all_s_r_o_combinations))) * len(
                all_s_r_o_combinations)

        s_r_o_comb = all_s_r_o_combinations[participant_number]
        s_comb = s_combinations[s_r_o_comb[0]]
        r_comb = r_combinations[s_r_o_comb[1]]

        if randomize_outcomes:
            o_comb = o_combinations[s_r_o_comb[2]]
        else:
            o_comb = list(range(self.total_outcomes))

        return s_comb, r_comb, o_comb

    def create_stims(self, order_objs, img_extension, init_str):
        stims = []

        for i, idx_img in enumerate(order_objs):
            img = self.load_img(idx_img, img_extension, init_str, self.stims_def_pos, self.stims_def_size)
            stim = Stim(self.psy, img)
            stims.append(stim)

        return stims

    def create_responses(self, order_objs, img_extension, init_str):
        responses = []

        for i, idx_img in enumerate(order_objs):
            img = self.load_img(idx_img, img_extension, init_str, self.responses_pos[i], self.responses_def_size[i])
            response = Response(self.psy, img)
            responses.append(response)

        return responses

    def create_outcomes(self, order_objs, img_extension, init_str):
        outcomes = []

        for i, idx_img in enumerate(order_objs):
            points = self.first_o_points[i] if i < len(self.first_o_points) else 0
            name = self.first_o_names[idx_img] if idx_img < len(self.first_o_names) else None
            size = self.outcomes_first_sizes[idx_img] if idx_img < len(self.outcomes_first_sizes) else self.outcomes_def_size

            img = self.load_img(idx_img, img_extension, init_str, self.outcomes_def_pos, size)
            outcome = Outcome(self.psy, img, name, points)
            outcomes.append(outcome)

        return outcomes

    def create_reload(self):
        reload_imgs = []

        for i in range(self.num_reload_imgs):
            reload_img_filename = "{}{}.png".format(self.reload_img_name, i+1)
            img_name = os.path.join(self.psy.dir_game_imgs, reload_img_filename)
            img = self.psy.load_img_from_disk(img_name, self.reload_pos, self.reload_def_size)
            reload_imgs.append(img)

        random.shuffle(reload_imgs)

        return reload_imgs

    def outcomes_change_pos_consumption(self, outcome_inds):
        for i, outcome_idx in enumerate(outcome_inds):
            pos = self.outcomes_pos_consumption_trial[i] if i < len(self.outcomes_pos_consumption_trial) else (0, 0)
            self.outcomes[outcome_idx].img.pos = pos
            self.outcomes[outcome_idx].img.draw()

        return 0

    def restore_outcomes_pos(self, outcome_inds):
        for i, outcome_idx in enumerate(outcome_inds):
            self.outcomes[outcome_idx].img.pos = self.outcomes_def_pos

    def show_text_points_info(self, accuracy, curr_block_type):
        if accuracy == 0:
            if curr_block_type == "dev":
                self.text_points_obj_suboptimal_dev.draw()
            else:
                self.text_points_obj_suboptimal.draw()
        elif accuracy == 1:
            if curr_block_type == "dev":
                self.text_points_obj_optimal_dev.draw()
            else:
                self.text_points_obj_optimal.draw()

    def load_img(self, img_idx, img_extension, init_str, pos=None, size=None):
        new_pos = (0, 0) if pos is None else pos
        new_size = (0, 0) if size is None else size

        img_name = os.path.join(self.psy.dir_game_imgs, init_str + str(img_idx + 1) + img_extension)
        img = self.psy.load_img_from_disk(img_name, new_pos, new_size)

        return img

class GameObj():
    def __init__(self, psy, img):
        self.img = img

class Stim(GameObj):
    def __init__(self, psy, img):
        super(Stim, self).__init__(psy, img)

class Response(GameObj):
    def __init__(self, psy, img):
        super(Response, self).__init__(psy, img)

class Outcome(GameObj):
    def __init__(self, psy, img, name, points=0, is_dev=False):
        super(Outcome, self).__init__(psy, img)

        self.name = name
        self.original_points = points
        self.points = points
        self.is_dev = is_dev
        self.create_points_info(psy)

    def create_points_info(self, psy):
        self.points_obj = visual.TextStim(win=psy.win, name='outcome_points',
                                          text='+ {}'.format(self.points),
                                          font='Arial',
                                          units='height', pos=(0, -0.1), height=0.04, wrapWidth=None, ori=0,
                                          color='white', colorSpace='rgb', opacity=1,
                                          languageStyle='LTR',
                                          depth=0.0)

    def devaluate(self, psy, new_points):
        self.points = new_points
        self.is_dev = True
        self.create_points_info(psy)

    def reset_dev(self, psy):
        self.points = self.original_points
        self.is_dev = False
        self.create_points_info(psy)

