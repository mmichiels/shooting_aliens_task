#import
import pandas as pd
import numpy as np
import hddm
from scipy import stats
import seaborn as sns
import matplotlib.pyplot as plt
import pymc
import kabuki
sns.set(style="white")
from tqdm import tqdm
import warnings
warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)

#load data. you will find this dataset in your hddm-folder under hddm/examples/rlddm_data.csv
# data = hddm.load_csv('datasets/rlddm_data.csv')
data = hddm.load_csv('datasets/current_aliens_dataset.csv')
#check structure
data.head()

#run the model by calling hddm.HDDMrl (instead of hddm.HDDM for normal HDDM)
m = hddm.HDDMrl(data, depends_on=None)
#set sample and burn-in
m.sample(100,burn=50)
#print stats to get an overview of posterior distribution of estimated parameters
m.print_stats()

# plot the posteriors of parameters
m.plot_posteriors()

traces = m.get_traces()
traces.head()

from tqdm import tqdm #progress tracker
#create empty dataframe to store simulated data
sim_data = pd.DataFrame()
#create a column samp to be used to identify the simulated data sets
data['samp'] = 0
#load traces
traces = m.get_traces()
#decide how many times to repeat simulation process. repeating this multiple times is generally recommended,
#as it better captures the uncertainty in the posterior distribution, but will also take some time
for i in tqdm(range(1,6)):
    #randomly select a row in the traces to use for extracting parameter values
    sample = np.random.randint(0,traces.shape[0]-1)
    #loop through all subjects in observed data
    for s in data.subj_idx.unique():
        #get number of trials for each condition.
        size0 = len(data[(data['subj_idx']==s) & (data['split_by']==0)].trial.unique())
        size1 = len(data[(data['subj_idx']==s) & (data['split_by']==1)].trial.unique())
        size2 = len(data[(data['subj_idx']==s) & (data['split_by']==2)].trial.unique())
        size3 = len(data[(data['subj_idx']==s) & (data['split_by']==3)].trial.unique())
        #set parameter values for simulation
        a = traces.loc[sample,'a_subj.'+str(s)]
        t = traces.loc[sample,'t_subj.'+str(s)]
        scaler = traces.loc[sample,'v_subj.'+str(s)]
        alphaInv = traces.loc[sample,'alpha_subj.'+str(s)]
        #take inverse logit of estimated alpha
        alpha = np.exp(alphaInv)/(1+np.exp(alphaInv))
        #simulate data for each condition changing only values of size, p_upper, p_lower and split_by between conditions.
        sim_data0 = hddm.generate.gen_rand_rlddm_data(a=a,t=t,scaler=scaler,alpha=alpha,size=size0,p_upper=1,p_lower=0,split_by=0)
        sim_data1 = hddm.generate.gen_rand_rlddm_data(a=a,t=t,scaler=scaler,alpha=alpha,size=size1,p_upper=1,p_lower=0,split_by=1)
        sim_data2 = hddm.generate.gen_rand_rlddm_data(a=a,t=t,scaler=scaler,alpha=alpha,size=size2,p_upper=1,p_lower=0,split_by=2)
        sim_data3 = hddm.generate.gen_rand_rlddm_data(a=a,t=t,scaler=scaler,alpha=alpha,size=size3,p_upper=1,p_lower=0,split_by=3)
        #append the conditions
        sim_data0 = sim_data0.append([sim_data1,sim_data2, sim_data3],ignore_index=True)
        #assign subj_idx
        sim_data0['subj_idx'] = s
        #identify that these are simulated data
        sim_data0['type'] = 'simulated'
        #identify the simulated data
        sim_data0['samp'] = i
        #append data from each subject
        sim_data = sim_data.append(sim_data0,ignore_index=True)
#combine observed and simulated data
ppc_data = data[['subj_idx','response','split_by','rt','trial','feedback','samp']].copy()
ppc_data['type'] = 'observed'
ppc_sdata = sim_data[['subj_idx','response','split_by','rt','trial','feedback','type','samp']].copy()
ppc_data = ppc_data.append(ppc_sdata)
# ppc_data.to_csv('ppc_data_tutorial.csv')

#for practical reasons we only look at the first 40 trials for each subject in a given condition
plot_ppc_data = ppc_data.copy()
# plot_ppc_data = ppc_data[ppc_data.trial<41].copy()

#set reaction time to be negative for lower bound responses (response=0)
plot_ppc_data['reaction time'] = np.where(plot_ppc_data['response']==1,plot_ppc_data.rt,0-plot_ppc_data.rt)
#plotting evolution of choice proportion for best option across learning for observed and simulated data. We use bins of trials because plotting individual trials would be very noisy.
g = sns.FacetGrid(plot_ppc_data,col='split_by',hue='type')
g.map(sns.kdeplot, 'reaction time',bw=0.05).set_ylabels("Density")
g.add_legend()
g.savefig('results_comp_models/rl_hddm_rt_data.svg')