from computational_models import CompModel as comp_model
import numpy as np

class RW(comp_model.CompModel):
    def __init__(self, psy):
        super(RW, self).__init__(psy)
        self.l_r = 0.1
        self.softmax_temp = 3  # Softmax temperature (higher temp -> more exploration; lower temp -> more exploitation)

        # Block-trials config (every block has num_trials_per_block; Total trials = num_blocks*num_trials_per_block)
        self.num_blocks = 9  #
        self.num_trials_per_block = 44 # Minimum recommended: 9 because it allows
        # stims_overtraining to appear 3.5 times more than stims_undertraining. Num_trials less than 22 can
        # cause num_trials to be a little different than the input number here, to try to adjust the number of stims
        # per trial according to the calculate_trials_stims_indices function
        self.num_trials_per_block_dev_mult = 1  # 2.72  # X times more than num_trials_per_block
        self.num_trials_per_block_dev = int(self.num_trials_per_block*self.num_trials_per_block_dev_mult)  # X% more than normal blocks  # 120
        self.dev_block_every = 99999
        self.num_consecutive_dev_blocks = 1
        self.consumption_trial_every = 99999
        self.trials_stims_indices = self.calculate_trials_stims_indices(self.num_trials_per_block, per_over_stim=0.5,
                                                                        times_over_stim=3.5, consumption_trials=True)
        self.trials_stims_indices_dev = self.calculate_trials_stims_indices(self.num_trials_per_block_dev, per_over_stim=0.5,
                                                                            times_over_stim=1, consumption_trials=True)
        self.blocks_config_indices = self.calculate_blocks_config_indices()

    def rw_equation(self, q_val_s_a, l_r, reward):
        # Rescorla-Wagner equation
        q_val_s_a = q_val_s_a + l_r * (reward - q_val_s_a)

        return q_val_s_a

    def action_selection_policy(self, stim_idx):
        # Max Q(s, a)
        q_vals_s = self.states_action_exp_reward[stim_idx]
        q_vals_s = np.array(list(q_vals_s.values()))
        q_vals_s_probs = comp_model.softmax(q_vals_s, self.softmax_temp)

        action_indices = list(range(self.objs.total_responses))
        action_idx = int(np.random.choice(action_indices, p=q_vals_s_probs))

        print("Probs actions", np.round(comp_model.softmax(q_vals_s, 0.1), decimals=2))
        print("Probs actions softmax", np.round(q_vals_s_probs, decimals=5))
        print("Vals actions", q_vals_s)

        return action_idx

    def update_q_val(self, stim_idx, action_idx, reward):
        q_val_s_a_prev = self.states_action_exp_reward[stim_idx][action_idx]

        q_val_s_a_new = self.rw_equation(q_val_s_a_prev, self.l_r, reward)
        self.states_action_exp_reward[stim_idx][action_idx] = q_val_s_a_new