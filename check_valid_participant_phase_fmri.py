import os
import pandas as pd
import numpy as np

def check(filename, results_participant, threshold=75, exclude_out_of_time_trials=True):
    valid = False
    points = results_participant["total_points"].iloc[-1]
    num_trials = len(results_participant)
    num_too_fast = (results_participant.loc[:, "response_in_time"] == 1).sum()
    num_too_slow = (results_participant.loc[:, "response_in_time"] == 2).sum()

    if exclude_out_of_time_trials:
        in_time_trials = results_participant.loc[:, "response_in_time"] == 0
        results_participant = results_participant[in_time_trials]
        num_trials = len(results_participant)

    num_correct = (results_participant.loc[:, "response_accuracy"] == 1).sum()
    percentage_correct = np.round(num_correct / num_trials * 100, 1)

    if percentage_correct >= threshold:
        valid = True
        print("Participant VALID (num_correct: {} of {}. Percentage correct : {} %; Points: {}) {} fast; {} slow --- {}".format(num_correct, num_trials,
                                                                                           percentage_correct, points, num_too_fast, num_too_slow, filename))
    else:
        print("Participant NOT valid (num_correct: {} of {}. Percentage correct : {} %; Points: {}) {} fast; {} slow --- {}".format(num_correct, num_trials,
                                                                                           percentage_correct, points, num_too_fast, num_too_slow, filename))

    return valid

if __name__ == "__main__":
    exclude_out_of_time_trials = True
    dir_results = os.path.join('.', 'results')
    results_file_prefix = "results"

    filenames_by_date = os.listdir(dir_results)
    filepaths_by_date = [os.path.join(dir_results, filename) for filename in filenames_by_date]
    filepaths_by_date.sort(key=lambda x: os.path.getmtime(x))

    print("------- EXCLUDE OUT OF TIME TRIALS : ", exclude_out_of_time_trials)

    for i, filename in enumerate(filenames_by_date):
        filepath = os.path.join(dir_results, filename)
        is_csv_results_file = filename.startswith(results_file_prefix) and filename.endswith('.csv')
        if is_csv_results_file and "_tmp" not in filename:
            results_participant = pd.read_csv(filepath, sep=',')
            check(filename, results_participant, threshold=75, exclude_out_of_time_trials=exclude_out_of_time_trials)
