import math

import utils_psychopy as utils_psy
import instructions as instr
import Experiment as exp
import multiprocessing

debug = False
instructions = False
in_fMRI = False
session_number = 3
participant_number = None

if __name__ == "__main__":
    psy_obj = utils_psy.Psychopy(debug, session_number, participant_number, in_fMRI)

    experiment = exp.Experiment(psy_obj)
    if instructions:
        instr.show_instructions(psy_obj, experiment.instruction_keys)

    experiment.run()

    psy_obj.save_results(experiment.results, format="csv")